/***
 * https://ecmascriptfeatures.online/
 * https://stackoverflow.com/questions/30076219/does-es6-introduce-a-well-defined-order-of-enumeration-for-object-properties/30919039#30919039
 * https://medium.com/weekly-webtips/most-important-javascript-features-in-es2020-es11-d8bd7ee80720
 * 
 */

function fun110() {
    var aFunction = function privateClass() {
        console.log("test")
        class Person {
            #born = 1980
            age() {
                console.log(2020 - this.#born)
                return this.#born
            }
        }
        const person1 = new Person()
        console.log(person1.age()) // 40
        //console.log(person1.#born)

        //Uncaught SyntaxError: Private field '#born' must be declared in an     
        //enclosing class
    };
    aFunction();
    loadOnHtml(aFunction);
}

fun111 = () => {
    var aFunction = function bigInt() {
        const big = 1000000n;
        console.log(Number(big) / 1000);

        console.log(Number.MAX_SAFE_INTEGER); // output is: 9007199254740991
        // Case (i):
        const maxNum = Number.MAX_SAFE_INTEGER;
        console.log(maxNum + 10); // output is: 9007199254741000 which is wrong.
        //Hence, when you implement BigInt by adding the alphabet ‘n’ at the end of your number, you will get the correct output/result.

        // Case (ii): When you add 10 to this number:
        const maxNum1 = 9007199254740991n;
        console.log(maxNum1 + 10n); // output  is: 9007199254741001n
        try {
            //Also, it is important to not mix BigInt with other types:
            const maxNum2 = 9007199254740991n;
            console.log(maxNum2 + 10); // output is: Cannot mix BigInt and other types, use explicit conversions
        } catch (e) {
            console.log(e)
        }
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun112 = () => {
    var aFunction = function dynamicImport() {
        /* let Dmodule;

        if ("module 1") {
            Dmodule = await import('./module1.js')
        } else {
            Dmodule = await import('./module2.js')
        }
        Dmodule.useMyModuleMethod()*/
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun113 = () => {
    var aFunction = function coalescing() {
        let theNumber = 7
        let number = theNumber || 5
        console.log(number) // output is: 7

        let theNumber1 = 0
        let number1 = theNumber1 || 5
        console.log(number1) // output is 5

        let theNumber2 = 0
        let number2 = theNumber2 ?? 5
        console.log(number2) // output is: 0

        let theNumber3 = null
        let number3 = theNumber3 ?? 5
        console.log(number3) // output is: 5
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun114 = () => {
    var aFunction = function optionalChaining() {
        const user = {
            "name": "Aryclenio Barros",
            "age": 22,
            "alive": true,
            "address": {
                "street": "Hyrule street",
                "number": 24,
            }
        }

        // Without optional chaining
        const number = user.address && user.address.number
        // With optional chaining
        const number1 = user.address?.number
        let smartphones = ['apple', 'samsung', 'motorola']
        console.log(smartphones[1]) // output is: samsung
        try {
            smartphones = null
            console.log(smartphones[1]) // output is: TypeError: Cannot read property '1' of null
        } catch (e) {
            console.log(e)
        }
        console.log(smartphones?.[1]) // output is: undefined

        let phoneApple = () => {
            return '11 Pro Max'
        }
        console.log(phoneApple()) // output is: 11 Pro Max
        try {
            phoneApple = null
            console.log(phoneApple()) // output is: TypeError: phoneApple is not a function
        } catch (e) {
            console.log(e)
        }
        console.log(phoneApple?.()) // undefined
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun115 = () => {
    var aFunction = function allSettled() {
        const myArrayOfPromises = [
            Promise.resolve(1),
            Promise.reject(0),
            Promise.resolve(2)
        ]

        Promise.allSettled(myArrayOfPromises).then((result) => {
            // Do your stuff
            console.log(result)
        })
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun116 = () => {
    var aFunction = function matchAll() {
        const regex = /\b(iPhone)+\b/g;
        const smartphones = "S series, iPhone, note series, iPhone, A series, iPhone, moto phones";
        for (const match of smartphones.matchAll(regex)) {
            console.log(match);
        }
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun117 = () => {
    var aFunction = function globalThis() {
        console.log(window == globalThis) // Browser
        try {
            console.log(global == globalThis) // Node js
        } catch (e) {
            console.log(e)
        }
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun118 = () => {
    var aFunction = function nameSpaceExports() {
        // Previously in JS
        /*import * as MyComponent from './Component.js'
        export {ns};

        // Added in ES11
        export * as MyComponent from './Component.js' */
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun119 = () => {
    var aFunction = function forInOrder() {
        var myArray = [
            {
                'key': 'key1',
                'value': 0
            },
            {
                'key': 'key2',
                'value': 1
            }
        ];

        for (let index in myArray) { console.log(myArray[index].value); }

        // Object Looping
        const object = { a: 1, b: 2, c: 3 };

        for (const property in object) {
            console.log(`${property}: ${object[property]}`);
        }
    };
    aFunction();
    loadOnHtml(aFunction);
}

loadOnHtml = (aFunction) => {
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
};