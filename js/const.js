//const
const testConst = "Hello Siva";
try {
    testConst = "test const reassign";
} catch (e) {
    console.log("error=============>", e.message)
}

const testConstArray = [1, 2, 3, 4];
testConstArray.push(5);
console.log(testConstArray);
try {
    testConstArray = [5, 6, 7, 8];
} catch (e) {
    console.log("error=============>", e.message)
}

const testConstObject = { firstName: "Siva", lastName: "Reddy" }
testConstObject.country = "India";
console.log(testConstObject)
try {
    testConstObject = { temp1: "hello", temp2: "siva" }
} catch (e) {
    console.log("error=============>", e.message)
}

// Const Scoping
checkConstScoping();
function checkConstScoping() {
    console.log("constant from checkConstScoping===> 1", constInsideFun);

    var constInsideFun = "Access me - functional scope";
    if (true) {
        //console.log("constant from checkConstScoping===> 2", constInsideFun);

        const constInsideFun = "Access me - block scope";
        console.log("constant from checkConstScoping===> 4", constInsideFun);

    }
    console.log("constant from checkConstScoping===> 3", constInsideFun);

}
