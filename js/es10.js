/***
 * Ref1: https://www.carloscaballero.io/12-es10-features-in-12-simple-examples/
 * 
 */

fun101 = () => {
    var aFunction = function trimStart() {
        console.log(' aaa '.trimStart());
        console.log(' aaa '.trimLeft());

        const greeting = '   Hello world!   ';
        console.log(greeting);
        // expected output: "   Hello world!   ";
        console.log(greeting.trimStart());
        // expected output: "Hello world!   ";
    };
    aFunction();
    loadOnHtml(aFunction);
}

fun102 = () => {
    var aFunction = function trimEnd() {
        const greeting = '   Hello world!   ';
        console.log(greeting);
        // expected output: "   Hello world!   ";
        console.log(greeting.trimEnd());
        // expected output: "   Hello world!";
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun103 = () => {
    var aFunction = function fromEntries() {

        //The Object.fromEntries() method transforms a list of key-value pairs into an object.
        const entries = new Map([
            ['foo', 'bar'],
            ['baz', 42]
        ]);

        const obj = Object.fromEntries(entries);

        console.log(obj);
        // expected output: Object { foo: "bar", baz: 42 }

    };
    aFunction();
    loadOnHtml(aFunction);
}
fun104 = () => {
    var aFunction = function flat() {
        //The Array.flat() method creates a new array with all sub-array elements 
        //concatenated into it recursively up to the specified depth.
        let arr = [1, 2, 3, [4, 5, 6, [7, 8, 9, [10, 11, 12]]]];
        console.log(arr.flat());
        console.log(arr.flat().flat()); // [1, 2, 3, 4, 5, 6, 7, 8, 9, Array(3)];
        console.log(arr.flat(3)); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        // Or, if you're not sure about the depth of the array:
        console.log(arr.flat(Infinity)); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]


        const arr1 = [0, 1, 2, [3, 4]];

        console.log(arr1.flat());
        // expected output: [0, 1, 2, 3, 4]

        const arr2 = [0, 1, 2, [[[3, 4]]]];

        console.log(arr2.flat(2));
        // expected output: [0, 1, 2, [3, 4]]
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun105 = () => {
    //The flatMap() method returns a new array formed by applying a given callback function to each element of the array, and then flattening the result by one level.
    var aFunction = function flatMap() {
        let arr = [1, 2, 3, 4, 5];
        console.log(arr.map(x => [x, x * 2]));
        console.log(arr.flatMap(x => [x, x * 2]));
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun106 = () => {
    var aFunction = function catchBinding() {
        let jsonData;
        try {
            jsonData = JSON.parse(str); // (A)
        } catch {
            jsonData = "";
        }
    };
    aFunction();
    loadOnHtml(aFunction);
}
fun107 = () => {
    var aFunction = function functionToString() {
        //The toString() method returns a string representing the source code of the function.
        function sum(a, b) {
            return a + b;
        }

        console.log(sum.toString());
        // expected output: "function sum(a, b) {
        //                     return a + b;
        //                   }"

        console.log(Math.abs.toString());
    };
    aFunction();
    loadOnHtml(aFunction);
}

loadOnHtml = (aFunction) => {
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
};