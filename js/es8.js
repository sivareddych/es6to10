/**
 * https://www.tutorialspoint.com/es6/es8_newfeatures.htm
 * 
 */

function fun081() {
    var aFunction = function padStart() {
        //pad the String with 0
        let product_cost = '1699'.padStart(7, 0)
        console.log(product_cost)
        console.log(product_cost.length)

        //pad the String with blank spaces
        let product_cost1 = '1699'.padStart(7)
        console.log(product_cost1)
        console.log(product_cost1.length)
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun082() {
    var aFunction = function padEnd() {
        //pad the string with x
        let product_cost = '1699'.padEnd(7, 'x')
        console.log(product_cost)
        console.log(product_cost.length)

        //pad the string with spaces
        let product_cost1 = '1699'.padEnd(7)
        console.log(product_cost1)
        console.log(product_cost1.length)
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun083() {
    var aFunction = function trailingCommas() {
        let marks = [100, 90, 80, ,]
        console.log(marks.length)
        console.log(marks)
        marks.forEach(function (e) { //ignores empty value in array
            console.log(e)
        })

        function sumOfMarks(marks,) { // trailing commas are ignored
            let sum = 0;
            marks.forEach(function (e) {
                sum += e;
            })
            return sum;
        }
        console.log(sumOfMarks([10, 20, 30]))
        console.log(sumOfMarks([1, 2, 3],))// trailing comma is ignored
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun084() {
    var aFunction = function objectEntriesAndValues() {
        const student = {
            firstName: 'Kannan',
            lastName: 'Sudhakaran'
        }
        console.log(Object.entries(student))
        console.log(Object.values(student))
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun085() {
    var aFunction = function asyncAwait() {
        function resolveAfter2Seconds() {
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve('resolved');
                }, 2000);
            });
        }

        async function asyncCall() {
            console.log('calling');
            const result = await resolveAfter2Seconds();
            console.log(result);
            // expected output: "resolved"
        }

        asyncCall();
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}
