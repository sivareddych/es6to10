/**
 * https://www.freecodecamp.org/news/ecmascript-2016-es7-features-86903c5cab70/
 * https://www.tutorialspoint.com/es6/es7_newfeatures.htm
 * 
 */

function fun071() {
    var aFunction = function exponention() {
        let base = 2
        let exponent = 3
        console.log('using Math.pow()', Math.pow(base, exponent))
        console.log('using exponentiation operator', base ** exponent)
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}


function fun072() {
    var aFunction = function arrayIncludes() {
        let marks = [50, 60, 70, 80]
        //check if 50 is included in array
        if (marks.includes(50)) {
            console.log('found element in array')
        } else {
            console.log('could not find element')
        }

        // check if 50 is found from index 1
        if (marks.includes(50, 1)) { //search from index 1
            console.log('found element in array')
        } else {
            console.log('could not find element')
        }

        //check Not a Number(NaN) in an array
        console.log([NaN].includes(NaN))

        let user1 = { name: 'kannan' },
            user2 = { name: 'varun' },
            user3 = { name: 'prijin' }
        let users = [user1, user2]

        //check object is available in array
        console.log(users.includes(user1))
        console.log(users.includes(user3))
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}
