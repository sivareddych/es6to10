/***
 *  https://ecmascriptfeatures.online/
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of
 * https://dmitripavlutin.com/object-rest-spread-properties-javascript/
 * 
 */

fun091 = () => {
    var aFunction = function forAwait() {
        function* generator() {
            yield 0;
            yield 1;
            yield Promise.resolve(2);
            yield Promise.resolve(3);
            yield 4;
        }

        (async function () {
            for await (let num of generator()) {
                console.log("await for of >>> ", num);
            }
        })();
        // 0
        // 1
        // 2
        // 3
        // 4

        // compare with for-of loop:

        for (let numOrPromise of generator()) {
            console.log("for of >>> ", numOrPromise);
        }
        // 0
        // 1
        // Promise { 2 }
        // Promise { 3 }
        // 4
    };
    aFunction();
    loadOnHtml(aFunction);
}

fun092 = () => {
    var aFunction = function restSpreadprop() {
        const cat = {
            legs: 4,
            sound: 'meow'
        };
        const dog = {
            ...cat,
            sound: 'woof'
        };

        console.log(dog); // => { legs: 4, sounds: 'woof' }
    };
    aFunction();
    loadOnHtml(aFunction);
}

fun093 = () => {
    var aFunction = function promiseFinally() {
        function checkMail() {
            return new Promise((resolve, reject) => {
                if (Math.random() > 0.5) {
                    resolve('Mail has arrived');
                } else {
                    reject(new Error('Failed to arrive'));
                }
            });
        }

        checkMail()
            .then((mail) => {
                console.log(mail);
            })
            .catch((err) => {
                console.error(err);
            })
            .finally(() => {
                console.log('Experiment completed');
            });

    };
    aFunction();
    loadOnHtml(aFunction);
}

fun094 = () => {
    var aFunction = function templateLiterally() {


        function tag(strings) {
            console.log(strings.raw[0]);
        }

        tag`string text line 1 \n string text line 2`;


        let person = 'Mike';
        let age = 28;

        function myTag(strings, personExp, ageExp) {
            let str0 = strings[0]; // "That "
            let str1 = strings[1]; // " is a "
            let str2 = strings[2]; // "."

            let ageStr;
            if (ageExp > 99) {
                ageStr = 'centenarian';
            } else {
                ageStr = 'youngster';
            }

            // We can even return a string built using a template literal
            return `${str0}${personExp}${str1}${ageStr}${str2}`;
        }

        let output = myTag`That ${person} is a ${age}.`;

        console.log(output);
        // That Mike is a youngster.
    };
    aFunction();
    loadOnHtml(aFunction);
}


loadOnHtml = (aFunction) => {
    document.getElementById("demo").innerHTML = `<code>${aFunction.toString()}</code>`;
};