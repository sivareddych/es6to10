/***
 * https://ecmascriptfeatures.online/
 * https://webapplog.com/es6/
 * https://www.tutorialrepublic.com/javascript-tutorial/javascript-es6-features.php
 * https://github.com/lukehoban/es6features
 * 
 */


function fun061() {
    var aFunction = function defaultParams() {
        function f(x, y = 12) {
            // y is 12 if not passed (or passed as undefined)
            return x + y;
        }
        console.log(f(3));
        console.log(f(3) == 15)
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun062() {
    var aFunction = function templateString() {
        // Basic literal string creation
        console.log(`In JavaScript \n is a line-feed.`)

        // Multiline strings
        console.log(`In JavaScript this is
          legal.`)

        // String interpolation
        var name = "Bob", time = "today";
        console.log(`Hello ${name}, how are you ${time}?`)
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

// Need to check again
function fun063() {
    var aFunction = function restAndspread() {
        function addNumbers(a, b, c) {
            return a + b + c;
        }
        let numbers = [5, 12, 8];

        // ES5 way of passing array as an argument of a function
        console.log(addNumbers.apply(null, numbers)); // 25

        // ES6 spread operator
        console.log(`spread operator --- ${addNumbers(...numbers)}`); // 25

        //The spread operator can also be used to insert the elements of an array into another array without using the array methods like push(), unshift() concat(), etc.
        let pets = ["Cat", "Dog", "Parrot"];
        let bugs = ["Ant", "Bee"];
        // Creating an array by inserting elements from other arrays
        let animals = [...pets, "Tiger", "Wolf", "Zebra", ...bugs];
        console.log(animals); // Cat,Dog,Parrot,Tiger,Wolf,Zebra,Ant,Bee

        // Rest
        function sortNames(...names) {
            return names.sort();
        }

        console.log(sortNames("Sarah", "Harry", "Peter")); // Harry,Peter,Sarah
        console.log(sortNames("Tony", "Ben", "Rick", "Jos")); // John,Jos,Rick,Tony
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun064() {
    var aFunction = function flat() {
        let fruits = ["Apple", "Banana"];

        let [a, b] = fruits; // Array destructuring assignment

        console.log(a); // Apple
        console.log(b); // Banana


        let fruits1 = ["Apple", "Banana", "Mango"];

        let [a1, ...r] = fruits1;

        console.log(a1); // Apple
        console.log(r); // Banana,Mango
        console.log(Array.isArray(r)); // true

        // ES6 syntax
        let person = { name: "Peter", age: 28 };

        let { name, age } = person; // Object destructuring assignment

        console.log(name); // Peter
        console.log(age); // 28
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun065() {
    var aFunction = function forOfAndforIn() {
        let letters = ["a", "b", "c", "d", "e", "f"];

        for (let letter of letters) {
            console.log(letter); // a,b,c,d,e,f
        }

        // Iterating over string
        let greet = "Hello World!";

        for (let character of greet) {
            console.log(character); // H,e,l,l,o, ,W,o,r,l,d,!
        }

        // An object with some properties 
        var person = { "name": "Clark", "surname": "Kent", "age": "36" };

        // Loop through all the properties in the object  
        for (var prop in person) {
            console.log(prop + " = " + person[prop]);
        }

        // An array with some elements
        var fruits = ["Apple", "Banana", "Mango", "Orange", "Papaya"];

        // Loop through all the elements in the array 
        for (var i in fruits) {
            console.log(fruits[i]);
        }

        //Note: The for-in loop should not be used to iterate over an array where the index order is important. You should better use a for loop with a numeric index.
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun066() {
    var aFunction = function arrowFunctions() {
        // Single parameter, single statement
        var greet = name => console.log("Hi " + name + "!");
        greet("Peter"); // Hi Peter!

        // Multiple arguments, single statement
        var multiply = (x, y) => x * y;
        console.log(multiply(2, 3)); // 6


        // Single parameter, multiple statements
        var test = age => {
            if (age > 18) {
                console.log("Adult");
            } else {
                console.log("Teenager");
            }
        }
        test(21); // Adult

        // Multiple parameters, multiple statements
        var divide = (x, y) => {
            if (y != 0) {
                return x / y;
            }
        }
        console.log(divide(10, 2)); // 5

        // No parameter, single statement
        var hello = () => console.log('Hello World!');
        hello(); // Hello World!
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun067() {
    var aFunction = function classes() {
        class Rectangle {
            // Class constructor
            constructor(length, width) {
                this.length = length;
                this.width = width;
            }

            // Class method
            getArea() {
                return this.length * this.width;
            }
        }

        // Square class inherits from the Rectangle class
        class Square extends Rectangle {
            // Child class constructor
            constructor(length) {
                // Call parent's constructor
                super(length, length);
            }

            // Child class method
            getPerimeter() {
                return 2 * (this.length + this.width);
            }
        }

        let rectangle = new Rectangle(5, 10);
        console.log(rectangle.getArea()); // 50

        let square = new Square(5);
        console.log(square.getArea()); // 25
        console.log(square.getPerimeter()); // 20

        console.log(square instanceof Square); // true
        console.log(square instanceof Rectangle); // true
        console.log(rectangle instanceof Square); // false
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun068() {
    var aFunction = function flat() {

    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun069() {
    var aFunction = function flat() {

    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun0610() {
    var aFunction = function flat() {

    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}
