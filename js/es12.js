/***
 * References:
 * 
 * https://carloscaballero.io/es2021-features-in-simple-examples/
 * 
 * https://codeburst.io/exciting-features-of-javascript-es2021-es12-1de8adf6550b
 *
 */

function fun121() {
    var aFunction = function replaceAll() {
        console.log('Cyan + magenta + yellow + black'.replaceAll('+', 'and'))
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

//More Study
function fun122() {

    var aFunction = async function promiseAny() {
        //Even though a promise is rejected earlier than a resolved promise, Promise.any() will return the first resolved promise
        Promise.any([
            new Promise((resolve, reject) => setTimeout(reject, 200, 'Third')),
            new Promise((resolve, reject) => setTimeout(resolve, 1000, 'Second')),
            new Promise((resolve, reject) => setTimeout(reject, 3000, 'Fourth')),
            new Promise((resolve, reject) => setTimeout(resolve, 4000, 'First')),
        ])
            .then(value => console.log(`Result: ${value}`))
            .catch(err => console.log(err));

        //When all the promises are rejected, AggregateError is thrown.

        Promise.any([
            Promise.reject('Error 1'),
            Promise.reject('Error 2'),
            Promise.reject('Error 3')
        ])
            .then(value => console.log(`Result: ${value}`))
            .catch(err => console.log(err))

    }
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun123() {
    var aFunction = function listFormat() {
        const arr = ['Pen', 'Pencil', 'Paper']

        let obj = new Intl.ListFormat('en', { style: 'short', type: 'conjunction' })
        console.log(obj.format(arr))

        /****  Output  ****/
        // Pen, Pencil, & Paper


        obj = new Intl.ListFormat('en', { style: 'long', type: 'conjunction' })
        console.log(obj.format(arr))

        /****  Output  ****/
        // Pen, Pencil, and Paper


        obj = new Intl.ListFormat('en', { style: 'narrow', type: 'conjunction' })
        console.log(obj.format(arr))

        /****  Output  ****/
        // Pen, Pencil, Paper


        // Passing in Italy language tag
        obj = new Intl.ListFormat('it', { style: 'short', type: 'conjunction' })
        console.log(obj.format(arr))

        /****  Output  ****/
        // Pen, Pencil e Paper


        // Passing in German language tag
        obj = new Intl.ListFormat('de', { style: 'long', type: 'conjunction' })
        console.log(obj.format(arr))

        /****  Output  ****/
        // Pen, Pencil und Paper


    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

function fun124() {
    var aFunction = function dateTimeFormat() {

        // Time only with short format
        let o = new Intl.DateTimeFormat('en', { timeStyle: 'short' })
        console.log(o.format(Date.now()))
        // 11:27 PM


        // Time only with medium format
        o = new Intl.DateTimeFormat('en', { timeStyle: 'medium' })
        console.log(o.format(Date.now()))
        // 11:27:57 PM


        // Time only with long format
        o = new Intl.DateTimeFormat('en', { timeStyle: 'long' })
        console.log(o.format(Date.now()))
        // 11:27:57 PM GMT+11


        // Date only with short format
        o = new Intl.DateTimeFormat('en', { dateStyle: 'short' })
        console.log(o.format(Date.now()))
        // 10/6/20


        // Date only with medium format
        o = new Intl.DateTimeFormat('en', { dateStyle: 'medium' })
        console.log(o.format(Date.now()))
        // Oct 6, 2020


        // Date only with long format
        o = new Intl.DateTimeFormat('en', { dateStyle: 'long' })
        console.log(o.format(Date.now()))
        // October 6, 2020

        let abc

        // English language
        abc = new Intl.DateTimeFormat('en', { timeStyle: 'short', dateStyle: 'long' })
        console.log(abc.format(Date.now()))
        // October 6, 2020 at 11:40 PM


        // Italian language
        abc = new Intl.DateTimeFormat('it', { timeStyle: 'short', dateStyle: 'long' })
        console.log(abc.format(Date.now()))
        // 6 ottobre 2020 23:40


        // German language
        abc = new Intl.DateTimeFormat('de', { timeStyle: 'short', dateStyle: 'long' })
        console.log(abc.format(Date.now()))
        // 6. Oktober 2020 um 23:40
    };
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

fun125 = () => {
    var aFunction = function logicalAssignmentOperator() {
        const a = { duration: 50, title: null };

        a.duration ||= 10;
        console.log("with || >>>>", a.duration);

        a.duration1 ||= 11;
        console.log("with || >>>>", a.duration1);

        a.duration &&= 10;
        console.log("with && >>>>", a.duration);

        a.duration2 &&= 13;
        console.log("with && >>>>", a.duration2);

        a.title ||= 'title is empty.';
        console.log(a.title);
        console.log("obj a>>>>>>", a)
        
    }

    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}

fun126 = () => {
    var aFunction = function numericSeparator() {
        var num = 1_000_000_000;
        console.log(typeof (num))
        var num1 = 2;
        console.log("1_000_000_000    ", num)
        console.log("res    ", num * 2)
    }
    aFunction();
    var functionText = '' + aFunction;
    document.getElementById("demo").innerHTML = `<code>${functionText}</code>`;
}