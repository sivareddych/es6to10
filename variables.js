/**
 * https://github.com/devsocial-project/es6-7-8-9-10-Cheatsheet
 * https://www.greycampus.com/blog/programming/java-script-versions
 * 
 * Lexical scoping -- https://dev.to/sandy8111112004/javascript-introduction-to-scope-function-scope-block-scope-d11
 * 
 */
//const
const testConst = "Hello Siva";
try {
    testConst = "test const reassign";
} catch (e) {
    console.log("error=============>", e.message)
}

const testConstArray = [1, 2, 3, 4];
testConstArray.push(5);
console.log(testConstArray);
try {
    testConstArray = [5, 6, 7, 8];
} catch (e) {
    console.log("error=============>", e.message)
}

const testConstObject = { firstName: "Siva", lastName: "Reddy" }
testConstObject.country = "India";
console.log(testConstObject)
try {
    testConstObject = { temp1: "hello", temp2: "siva" }
} catch (e) {
    console.log("error=============>", e.message)
}

// Const Scoping
checkConstScoping();
function checkConstScoping() {
    console.log("constant from checkConstScoping===> 1", constInsideFun);

    var constInsideFun = "Access me - functional scope";
    if (true) {
        //console.log("constant from checkConstScoping===> 2", constInsideFun);

        const constInsideFun = "Access me - block scope";
        console.log("constant from checkConstScoping===> 4", constInsideFun);

    }
    console.log("constant from checkConstScoping===> 3", constInsideFun);

}

// let

let letVariable = "Hello Team!";
/** 
 * 
 * https://javascript.plainenglish.io/how-hoisting-works-with-let-and-const-in-javascript-725616df7085
 * https://www.vojtechruzicka.com/javascript-hoisting-var-let-const-variables/
*/
//var declarations are globally scoped or function scoped while let and const are block scoped. 
//var variables can be updated and re-declared within its scope; 
//let variables can be updated but not re-declared; const variables can neither be updated nor re-declared.

checkletScoping();
function checkletScoping() {
    console.log("===> 1", letInsideFun);

    var letInsideFun = "Access me - functional scope";
    if (true) {
        // console.log("===> 2", constInsideFun);

        let letInsideFun = "Access me - block scope";

        console.log("===> 3", letInsideFun);
        letInsideFun = "Redfined"
        console.log("===> 4", letInsideFun);


    }
    console.log("===> 5", letInsideFun);

    for (var i = 0; i < 3; i++) {
        console.log("with var ==>", i);
        setTimeout(function () { // this event register 3 times
            console.log("The number is var " + i);
        }, 0);
    };
    console.log("outside for var lopp ==>", i);

    for (let i = 0; i < 3; i++) {
        setTimeout(function () {
            console.log("The number is let " + i);
        }, 0);
    }

}